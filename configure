##########################################################################
# Copyright © 2011-2015 Vincent Prat & Simon Nicolas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##########################################################################

#!/bin/sh

# Trim function
trim()
{
    readlink -m $(echo $1 | tr -d "'" | sed "s#^~#$HOME#" | sed "s#\${prefix}#$prefix#")
}

# target name
target="gm-assistant"

# Initializing some options
debug=0
system_install=1
build=0

# Error returns
err_bad_option=1
err_bad_os=2
err_missing_soft=3
err_missing_header=4

# default paths
default_prefix="/usr"
default_bindir="games"
default_datadir="share/games/$target"
default_desktopdir="share/applications"
default_mandir="share/man"
default_sysconfdir="/etc"
default_usrconfdir="$HOME"

# file names
pro_file=$target.pro
desk_file=$target.desktop
conf_file=$target.conf
man_file=$target.6

# Reading options
supported_short_options="ghu"
supported_long_options="help,debug,prefix:,user,bindir:,datadir:,desktopdir:,build:,includedir:,mandir:,libdir:,infodir:,sysconfdir:,usrconfdir:,localstatedir:,libexecdir:,disable-maintainer-mode,disable-dependency-tracking,disable-silent-rules"

options=$(getopt -s sh -l $supported_long_options $supported_short_options $@)
if [ $? -eq 1 ]   # unsupported options
then
    echo "Try the --help option for more information"
    exit $err_bad_option
fi

# Test of the behavior of echo
if [ "$(echo -e)" = '-e' ]
then
    echo_e=''
else
    echo_e='-e'
fi

# initialization
arg=0
for opt in $options
do
    if [ $arg -eq 0 -o "$opt" = "--" ]
    then
        case $opt in
            -g | --debug)   debug=1;;
            -h | --help)    echo $echo_e "Usage: configure [options]\n\nGeneral options:\n -g, --debug\tEnable debugging\n -h, --help\tDisplay this usage guide\n -u, --user\tInstall in a user scope\n\nDirectory options:\n --prefix=[$default_prefix]\t\t\t\tInstallation prefix\n --bindir=[<prefix>/$default_bindir]\t\t\tBinary directory\n --datadir=[<prefix>/$default_datadir]\tData directory\n --desktopdir=[<prefix>/$default_desktopdir]\tDesktop file directory\n --mandir=[<prefix>/$default_mandir]\t\t\tManual directory\n --sysconfdir=[$default_sysconfdir]\t\t\t\tSystem configuration directory\n --usrconfdir=[$default_usrconfdir]\t\t\tUser configuration directory\n\nEnvironment variables:\n CFLAGS:\tC compilation flags\n CXXFLAGS:\tC++ compilation flags\n CPPFLAGS:\tC/C++ pre-processor flags\n LDFLAGS:\tlinking flags"
                exit 0;;
            -u | --user)    system_install=0;;
            --build)        build=1;;
            --prefix)       arg=1;;
            --sysconfdir)   arg=2;;
            --mandir)       arg=3;;
            --bindir)       arg=4;;
            --datadir)      arg=5;;
            --desktopdir)   arg=6;;
            --usrconfdir)   arg=7;;
            --) break;;
        esac
    else
        case $arg in
            1)  prefix=$(trim $opt);;
            2)  sysconfdir=$(trim $opt);;
            3)  mandir=$(trim $opt);;
            4)  bindir=$(trim $opt);;
            5)  datadir=$(trim $opt);;
            6)  desktopdir=$(trim $opt);;
            7)  usrconfdir=$(trim $opt);;
        esac
        arg=0
    fi
done

# Setting paths
if [ -z $prefix ]
then
    prefix="$default_prefix"
fi
if [ -z $bindir ]
then
    bindir="$prefix/$default_bindir"
fi
if [ -z $datadir ]
then
    datadir="$prefix/$default_datadir"
fi
if [ -z $desktopdir ]
then
    desktopdir="$prefix/$default_desktopdir"
fi
if [ -z $mandir ]
then
    mandir="$prefix/$default_mandir"
fi
if [ $system_install -ne 0 ]
then
    if [ -z $sysconfdir ]
    then
        sysconfdir="$default_sysconfdir"
    fi
    confdir="$sysconfdir/xdg/$target"
else
    if [ -z $usrconfdir ]
    then
        usrconfdir="$default_usrconfdir"
    fi
    confdir="$usrconfdir/.config/$target"
fi
mandir="$mandir/man6"


# Find the OS
OS=$(uname)
echo "******************************"
echo "* Detecting operating system *"
echo "******************************"
echo
echo Operating system: $OS
# OS-dependent variables
if [ -n $(which qmake) ]
then
    qmake="qmake"
    lrelease="lrelease"
    lupdate="lupdate"
    moc="moc"
    uic="uic"
elif [ -n $(which qmake-qt4) ]
then
    qmake="qmake-qt4"
    lrelease="lrelease-qt4"
    lupdate="lupdate-qt4"
    moc="moc-qt4"
    uic="uic-qt4"
else
    echo Both qmake and qmake-qt4 missing !
    exit $err_missing_soft
fi


if [ $OS = "Linux" ]
then
	inc_path="/usr/include"
    sed_i="-i"
elif [ $OS = "Darwin" ]
then
	inc_path="/opt/local/include"
    sed_i='-i ""'
else
    echo "Operating system not supported."
    exit $err_bad_os
fi

echo
echo "*************************"
echo "* Checking softwares... *"
echo "*************************" 
echo

# Needed softwares
softs="uname echo getopt ls grep head pkg-config make sed install tr readlink $qmake $lrelease $lupdate g++ $moc rcc $uic"
if [ $build -ne 0 ]
then
    softs=$softs" rsvg-convert convert"
fi

for soft in $softs
do
    path=$(which $soft)
    if [ $? = 1 ]
    then
        echo $soft missing !
        exit $err_missing_soft
    else
        echo $soft: $path        
    fi
done

# Optional software
path=$(which dot)
if [ $? = 1 ]
then
    dot="NO"
else
    dot="YES"
    echo dot: $path        
fi
sed $sed_i "s/HAVE_DOT.*=.*/HAVE_DOT\t\t= $dot/" dox.conf

echo
echo "*********************************"
echo "* Checking development files... *"
echo "*********************************"

# Needed headers

# libXML++
echo
echo libXML++:
xml_devs="libxml++"
xml_dev_path=$(ls -l $inc_path | grep -o -e "libxml++-2\.[0-9]\{1,\}" | head -n 1)

for dev in $xml_devs
do
    if [ -e "$inc_path/$xml_dev_path/libxml++/$dev.h" ]
    then
        echo $inc_path/$xml_dev_path/libxml++/$dev.h: OK
    else
        echo $dev.h missing !
        exit $err_missing_header
    fi
done

# Phonon
echo
echo Phonon:
phonon_devs="phonon"
phonon_dev_path="$inc_path/phonon"

for dev in $phonon_devs
do
    if [ -e "$phonon_dev_path/$dev" ]
    then
        echo $phonon_dev_path/$dev: OK
    else
        echo $dev missing !
        exit $err_missing_header
    fi
done

# magic
echo
echo magic:
magic_devs="magic"
magic_dev_path=$inc_path

for dev in $magic_devs
do
    if [ -e "$magic_dev_path/$dev.h" ]
    then
        echo $magic_dev_path/$dev.h: OK
    else
        echo $dev.h missing !
        exit $err_missing_header
    fi
done

# PocoZip
echo
echo PocoZip:
pocozip_devs="Zip"
pocozip_dev_path="$inc_path/Poco/Zip"

for dev in $pocozip_devs
do
    if [ -e "$pocozip_dev_path/$dev.h" ]
    then
        echo $pocozip_dev_path/$dev.h: OK
    else
        echo $dev.h missing !
        exit $err_missing_header
    fi
done

echo
echo "********************************"
echo "* Preparing the compilation... *"
echo "********************************"
echo

$qmake -project -o $pro_file
echo "QT += svg phonon" >> $pro_file
$lupdate -no-obsolete $pro_file
$lrelease $pro_file
$qmake $pro_file

# adding the -g flag if the debug option is enabled
if [ $debug -ne 0 ]
then
    sed $sed_i "s#-pipe#-pipe -g#" Makefile
fi

echo
echo "************************"
echo "* Adding librairies... *"
echo "************************"
echo

echo $xml_dev_path
echo Phonon
echo magic
echo PocoZip

# adding includes in the Makefile
sed $sed_i "s#-I\.#$(pkg-config $xml_dev_path --cflags-only-I) -I$magic_dev_path -I$pocozip_dev_path#" Makefile

# adding libraries in the Makefile
sed $sed_i "s#-lQtCore#-lQtCore $(pkg-config $xml_dev_path --libs) -lmagic -lPocoZip -lPocoFoundation#" Makefile

# cleaning previous compilation
make -s clean
rm -f *.o
rm -f ui_*.h

# add compilation and link flags from environment variables
sed $sed_i 's#CFLAGS.*=\(.*\)$(DEFINES)#CFLAGS += \1$(DEFINES) $(CPPFLAGS)#' Makefile
sed $sed_i 's#CXXFLAGS.*=\(.*\)$(DEFINES)#CXXFLAGS += \1$(DEFINES) $(CPPFLAGS)#' Makefile
sed $sed_i 's#LFLAGS.*=\(.*\)#LFLAGS = \1 $(LDFLAGS)#' Makefile

# add files to clean
sed $sed_i 's#DEL_FILE) $(OBJECTS)#DEL_FILE) $(OBJECTS)\n\t-$(DEL_FILE) GMA.*#' Makefile
sed $sed_i 's#DEL_FILE) Makefile#DEL_FILE) Makefile\n\t-$(DEL_FILE) translations/*.qm\n\t-$(DEL_FILE) '$pro_file'\n\t-$(DEL_FILE) '$conf_file'\n\t-$(DEL_FILE) '$desk_file'#' Makefile
sed $sed_i 's#DEL_FILE) $(OBJECTS)#DEL_FILE) $(OBJECTS)\n\t-$(DEL_FILE) doc/userguide/userguide-*\n\t-$(DEL_DIR) doc/dev#' Makefile

# preparing conf file
echo "[directories]" > $conf_file
echo "install=$datadir" >> $conf_file

# preparing the installation
if [ $system_install -ne 0 ]
then
    sed $sed_i 's#-o main.o#-DSYSTEM_INSTALL -o main.o#' Makefile
fi

if [ $build -ne 0 ]
then
    # preparing the icon
    sed $sed_i 's#Makefile $(TARGET)#Makefile $(TARGET) GMA.xpm doc\n\nGMA.xpm: GMA.png\n\tconvert -resize 32x32 $< $@\n\nGMA.png: data/images/GMA.svg\n\trsvg-convert $< -o $@#' Makefile
    install_icon='\t$(INSTALL_FILE) GMA.xpm $(DATADIR)\n'
fi

# preparing the desktop file
sed "s#<bin_path>#$bindir#" $desk_file.src > $desk_file 
sed $sed_i "s#<icon_path>#$datadir#" $desk_file

sed $sed_i 's#^install:.*#BASEDIR=$(DESTDIR)'$prefix'\nBINDIR=$(DESTDIR)'$bindir'\nDATADIR=$(DESTDIR)'$datadir'\nTRANSLATIONS=$(DATADIR)/translations\nMANDIR=$(DESTDIR)'$mandir'\nCONFDIR=$(DESTDIR)'$confdir'\nDESKTOPDIR=$(DESTDIR)'$desktopdir'\n\ninstall:\n\tinstall -d $(BASEDIR) $(BINDIR) $(TRANSLATIONS) $(MANDIR) $(CONFDIR) $(DESKTOPDIR)\n\t$(INSTALL_PROGRAM) '$target' $(BINDIR)\n'"$install_icon"'\t$(INSTALL_FILE) data/images/GMA.svg $(DATADIR)\n\t$(INSTALL_FILE) translations/*.qm translations/languages $(TRANSLATIONS)\n\t$(INSTALL_FILE) doc/'$man_file' $(MANDIR)\n\t$(INSTALL_FILE) '$conf_file' $(CONFDIR)\n\t$(INSTALL_FILE) '$desk_file' $(DESKTOPDIR)#' Makefile

# preparing the uninstallation
sed $sed_i 's#rmdir#rmdir --ignore-fail-on-non-empty#' Makefile
sed $sed_i 's#^uninstall:.*#uninstall:\n\t$(DEL_FILE) $(BINDIR)/'$target'\n\t$(DEL_DIR) $(BINDIR)\n\t$(DEL_FILE) $(TRANSLATIONS)/*\n\t$(DEL_DIR) $(TRANSLATIONS)\n\t$(DEL_FILE) $(DATADIR)/GMA.*\n\t$(DEL_DIR) $(DATADIR)\n\t$(DEL_FILE) $(MANDIR)/'$man_file'\n\t$(DEL_DIR) $(MANDIR)\n\t$(DEL_FILE) $(CONFDIR)/'$conf_file'\n\t$(DEL_DIR) $(CONFDIR)\n\t$(DEL_FILE) $(DESKTOPDIR)/'$desk_file'\n\t$(DEL_DIR) $(DESKTOPDIR)#' Makefile

echo
echo "********************"
echo "* Ready to compile *"
echo "********************"
echo

# adding the rule for generating the documentation
pdfl=$(which pdflatex)
if [ $? = 0 ]
then
    echo "To generate the user documentation, type make doc"
    echo ".PHONY: doc" >> Makefile
    list=""
    for dir in doc/userguide/*
    do
        lang=$(echo $dir | sed "s#doc/userguide/##")
        file="$dir/userguide.tex"
        outputfile="doc/userguide/userguide-$lang.pdf"
        if [ -e $file ]
        then
            list="$list $outputfile"
            echo $echo_e $outputfile': '$file'\n\tcd '$dir'; pdflatex userguide.tex; pdflatex userguide.tex\n\tcp '$dir'/userguide.pdf doc/userguide/userguide-'$lang'.pdf\n' >> Makefile
        fi
    done
    echo $echo_e 'doc: '$list'\n' >> Makefile
    echo $echo_e 'DOCDIR=$(DESTDIR)'$prefix'/share/doc/gm-assistant-doc/' >> Makefile
    echo $echo_e '\ninstall-doc:\n\tinstall -d $(DOCDIR)\n\t$(INSTALL_FILE) doc/userguide/userguide-*.pdf $(DOCDIR)\n' >> Makefile
    echo $echo_e 'uninstall-doc:\n\t$(DEL_FILE) $(DOCDIR)/userguide-*.pdf\n\t$(DEL_DIR) $(DOCDIR)\n' >> Makefile
    if [ $build -ne 0 ]
    then
        sed $sed_i 's#^install:.*#install: install-doc#' Makefile
        sed $sed_i 's#^uninstall:.*#uninstall: uninstall-doc#' Makefile
    fi
else
    echo "In order to be able to generate the user documentation, you need pdflatex"
    echo
fi

dox=$(which doxygen)
if [ $? = 0 ]
then
    echo "To generate the developer documentation, type make devdoc"
    echo $echo_e 'devdoc: dox.conf $(filter sources/%,$(SOURCES))\n\tdoxygen dox.conf' >> Makefile
else
    echo "In order to be able to generate the developer documentation, you need doxygen"
    echo
fi

echo "To compile, type make"
