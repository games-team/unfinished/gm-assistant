<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="17"/>
        <source>About GM-Assistant</source>
        <translation>À propos de GM-Assistant</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="26"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;GM-Assistant&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="42"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p&gt;Version:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//FR&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p&gt;Version :&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="77"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p style=&quot;text-decoration: underline&quot;&gt;Developed and maintained by:&lt;/p&gt;
&lt;p&gt;Simon Nicolas &amp;amp; Vincent Prat&lt;br /&gt;
&lt;a href=&quot;http://gmassistant.free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://gmassistant.free.fr&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p style=&quot;text-decoration: underline&quot;&gt;Developpé et maintenu par :&lt;/p&gt;
&lt;p&gt;Simon Nicolas &amp;amp; Vincent Prat&lt;br /&gt;
&lt;a href=&quot;http://gmassistant.free.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://gmassistant.free.fr&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="91"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Description:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GM-Assistant is a free software designed to assist the Game Master during role-playing games.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Description:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GM-Assistant est un logiciel libre conçu pour assister le Maître du Jeu lors de jeux de rôle.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="116"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Using:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Qt4&lt;/span&gt;&lt;/a&gt; with the Phonon module&lt;br /&gt;&lt;a href=&quot;http://libxmlplusplus.sourceforge.net/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;libxml++&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;
&lt;a href=&quot;http://pocoproject.org/&quot;&gt;Poco&lt;/a&gt;
&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Utilisant :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Qt4&lt;/span&gt;&lt;/a&gt; avec le module Phonon&lt;br /&gt;&lt;a href=&quot;http://libxmlplusplus.sourceforge.net/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;libxml++&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;
&lt;a href=&quot;http://pocoproject.org/&quot;&gt;Poco&lt;/a&gt;
&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="106"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p style=&quot; text-decoration: underline;&quot;&gt;License:&lt;/p&gt;
&lt;p&gt;GPLv3+&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//FR&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;body&gt;
&lt;p style=&quot; text-decoration: underline;&quot;&gt;Licence :&lt;/p&gt;
&lt;p&gt;GPLv3+&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../sources/windows/AboutDialog.ui" line="66"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>changeCharacterDialog</name>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.ui" line="17"/>
        <source>New name</source>
        <translation>Nouveau nom</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.ui" line="29"/>
        <source>Character&apos;s name:</source>
        <translation>Nom du personnage :</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.ui" line="39"/>
        <source>Short description:</source>
        <translation>Description rapide :</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.ui" line="51"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.ui" line="62"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.cpp" line="36"/>
        <source>Uncomplete data</source>
        <translation>Données incomplètes</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangeCharacterDialog.cpp" line="36"/>
        <source>You must fill the content before validating.</source>
        <translation>Vous devez remplir le contenu avant de valider.</translation>
    </message>
</context>
<context>
    <name>changePropertyDialog</name>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.ui" line="17"/>
        <source>New name</source>
        <translation>Nouveau nom</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.ui" line="29"/>
        <source>Name of the property:</source>
        <translation>Nom de la propriété :</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.ui" line="41"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.ui" line="52"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.cpp" line="41"/>
        <source>Uncomplete data</source>
        <translation>Données incomplètes</translation>
    </message>
    <message>
        <location filename="../sources/windows/ChangePropertyDialog.cpp" line="41"/>
        <source>You must fill the content before validating.</source>
        <translation>Vous devez remplir le contenu avant de valider.</translation>
    </message>
</context>
<context>
    <name>combatDialog</name>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="14"/>
        <source>Combat manager</source>
        <translation>Gestionnaire de combat</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="37"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="47"/>
        <source>&amp;Next</source>
        <translation>&amp;Suivant</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="57"/>
        <source>&amp;Remove</source>
        <translation>S&amp;upprimer</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="64"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.ui" line="20"/>
        <location filename="../sources/windows/CombatDialog.cpp" line="70"/>
        <source>Current character:</source>
        <translation>Personnage actuel :</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.cpp" line="102"/>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <location filename="../sources/windows/CombatDialog.cpp" line="102"/>
        <source>You are about to remove %1 from the combat manager. Are you sure you want to do it?</source>
        <translation>Vous êtes sur le point de retirer %1 du gestionnaire de combat. Êtes-vous sûr de vouloir le faire ?</translation>
    </message>
</context>
<context>
    <name>customTable</name>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="717"/>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="727"/>
        <source>&amp;Add</source>
        <translation>&amp;Ajouter</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="729"/>
        <source>Ctrl+Shift+Ins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="732"/>
        <source>Ctrl+Shift+Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="735"/>
        <source>Ctrl+Shift+F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="719"/>
        <source>Ctrl+Ins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="716"/>
        <source>&amp;Property</source>
        <translation>&amp;Propriété</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="718"/>
        <source>Add a new property</source>
        <translation>Ajoute une nouvelle propriété</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="720"/>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="730"/>
        <source>&amp;Remove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="722"/>
        <source>Ctrl+Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="723"/>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="733"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="721"/>
        <source>Remove the property</source>
        <translation>Supprime la propriété</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="724"/>
        <source>Edit the property</source>
        <translation>Édite la propriété</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="725"/>
        <source>Ctrl+F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="726"/>
        <source>&amp;Character</source>
        <translation>&amp;Personnage</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="728"/>
        <source>Add a new character</source>
        <translation>Ajoute un nouveau personnage</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="731"/>
        <source>Remove the character</source>
        <translation>Supprime le personnage</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTableWidget.cpp" line="734"/>
        <source>Edit the character</source>
        <translation>Édite le personnage</translation>
    </message>
</context>
<context>
    <name>customTree</name>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="750"/>
        <source>&amp;None</source>
        <translation>&amp;Aucun</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="751"/>
        <source>Untag the item</source>
        <translation>Retire toute étiquette de l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="752"/>
        <source>Ctrl+F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="753"/>
        <source>In &amp;progress</source>
        <translation>En &amp;cours</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="754"/>
        <source>Tag the item as being in progress</source>
        <translation>Marque l&apos;item comme étant en cours</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="755"/>
        <source>Ctrl+F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="756"/>
        <source>&amp;Failed</source>
        <translation>&amp;Échoué</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="757"/>
        <source>Tag the item as failed</source>
        <translation>Marque l&apos;item comme échoué</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="758"/>
        <source>Ctrl+F7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="759"/>
        <source>&amp;Succeeded</source>
        <translation>&amp;Réussi</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="760"/>
        <source>Tag the item as succeeded</source>
        <translation>Marque l&apos;item comme réussi</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="761"/>
        <source>Ctrl+F8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="762"/>
        <source>&amp;Add</source>
        <translation>A&amp;jouter</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="763"/>
        <source>Add a new item</source>
        <translation>Ajoute un nouvel item</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="764"/>
        <source>Ins</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="765"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="766"/>
        <source>Delete the item</source>
        <translation>Supprime l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="767"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="768"/>
        <source>&amp;Edit</source>
        <translation>É&amp;diter</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="769"/>
        <source>Edit the item</source>
        <translation>Édite l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="770"/>
        <source>Ctrl+F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="771"/>
        <source>Space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="772"/>
        <source>E&amp;xport</source>
        <translation>E&amp;xporter</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="773"/>
        <source>Export the file associated to the item</source>
        <translation>Exporte le fichier associé à l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="774"/>
        <source>Ctrl+Space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="802"/>
        <source>Select where to export the file</source>
        <translation>Sélectionnez où exporter le fichier</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="807"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="807"/>
        <source>Unable to export the file</source>
        <translation>Impossible d&apos;exporter le fichier</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="95"/>
        <source>Audio files can be played only in the Music and Sound effects modules.</source>
        <translation>Les fichiers audios ne peuvent être joués que dans les modules Musique et Bruitages.</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="145"/>
        <source>Play the audio file</source>
        <translation>Joue le fichier audio</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="109"/>
        <source>Unable to display the file</source>
        <translation>Impossible d&apos;afficher le fichier</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="146"/>
        <source>P&amp;lay</source>
        <translation>J&amp;ouer</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="151"/>
        <source>Display the image</source>
        <translation>Affiche l&apos;image</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="152"/>
        <source>Disp&amp;lay</source>
        <translation>A&amp;fficher</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidgetItem.cpp" line="60"/>
        <source> (included in the loaded file)</source>
        <translation> (inclus dans le fichier chargé)</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidgetItem.cpp" line="71"/>
        <source>Double click to play the file</source>
        <translation>Double-cliquez pour jouer le fichier</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidgetItem.cpp" line="75"/>
        <source>Double click to show the file</source>
        <translation>Double-cliquez pour afficher l&apos;image</translation>
    </message>
</context>
<context>
    <name>diceDialog</name>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="14"/>
        <source>Dice simulator</source>
        <translation>Simulateur de dés</translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="28"/>
        <source>Die type:</source>
        <translation>Type de dé :</translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="42"/>
        <source>d2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="47"/>
        <source>d3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="52"/>
        <source>d4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="57"/>
        <source>d6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="62"/>
        <source>d8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="67"/>
        <source>d10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="72"/>
        <source>d12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="77"/>
        <source>d20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="82"/>
        <source>d100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="90"/>
        <source>Number of dice:</source>
        <translation>Nombre de dés :</translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="118"/>
        <source>&amp;Throw</source>
        <translation>&amp;Lancer</translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="128"/>
        <source>&amp;Reset</source>
        <translation>&amp;Mettre à zéro</translation>
    </message>
    <message>
        <location filename="../sources/windows/DiceDialog.ui" line="135"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>itemDialog</name>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="17"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="179"/>
        <source>Create a new item</source>
        <translation>Créer un nouvel item</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="29"/>
        <source>Content</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="41"/>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="47"/>
        <source>&amp;None</source>
        <translation>&amp;Aucun</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="61"/>
        <source>&amp;In progress</source>
        <translation>En &amp;cours</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="72"/>
        <source>&amp;Failed</source>
        <translation>&amp;Échoué</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="83"/>
        <source>&amp;Succeeded</source>
        <translation>&amp;Réussi</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="97"/>
        <source>Type</source>
        <translation>Type
</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="103"/>
        <source>&amp;Basic</source>
        <translation>&amp;Basique</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="117"/>
        <source>A&amp;udio</source>
        <translation>A&amp;udio</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="128"/>
        <source>I&amp;mage</source>
        <translation>&amp;Image</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="141"/>
        <source>File:</source>
        <translation>Fichier :</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="158"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="172"/>
        <source>&amp;Add</source>
        <translation>A&amp;jouter</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="183"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="182"/>
        <source>C&amp;hild</source>
        <translation>En&amp;fant</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.ui" line="194"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="212"/>
        <source>Audio files (*)</source>
        <translation>Fichiers audio (*)</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="85"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="90"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="105"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="110"/>
        <source>Uncomplete data</source>
        <translation>Données incomplètes</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="85"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="105"/>
        <source>You must select a file before validating.</source>
        <translation>Vous devez sélectionner un fichier avant de valider.</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="90"/>
        <location filename="../sources/windows/ItemDialog.cpp" line="110"/>
        <source>You must fill the content before validating.</source>
        <translation>Vous devez remplir le contenu avant de valider.</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="148"/>
        <source>Select the image file to associate to the item</source>
        <translation>Sélectionnez le fichier image à associer à l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="22"/>
        <source>Select the audio file to associate to the item</source>
        <translation>Sélectionnez le fichier audio à associer à l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="148"/>
        <source>Image files (*.jpg *.jpeg *.png *.bmp *.svg)</source>
        <translation>Fichiers image (*.jpg *.jpeg *.png *.bmp *.svg)</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="163"/>
        <source>Edit the item</source>
        <translation>Éditer l&apos;item</translation>
    </message>
    <message>
        <location filename="../sources/windows/ItemDialog.cpp" line="166"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="69"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="155"/>
        <source>Sound effects</source>
        <translation>Bruitages</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="197"/>
        <source>Music</source>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="14"/>
        <source>GM-Assistant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="27"/>
        <source>Plot</source>
        <translation>Intrigue</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="111"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="133"/>
        <source>Characters</source>
        <translation>Personnages</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="238"/>
        <source>Browse the music file</source>
        <translation>Navigue dans le fichier musical</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="247"/>
        <source>Play/Pause/Resume the music</source>
        <translation>Joue/pause/reprend la musique</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="250"/>
        <location filename="../sources/windows/MainWindow.cpp" line="484"/>
        <source>&amp;Play</source>
        <translation>&amp;Lecture</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="257"/>
        <source>0:00/0:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="267"/>
        <source>Enable/Disable loop playing</source>
        <translation>Active/désactive le jeu en boucle</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="270"/>
        <source>&amp;Loop</source>
        <translation>&amp;Boucle</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="296"/>
        <source>&amp;Game</source>
        <translation>&amp;Jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="319"/>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="326"/>
        <source>&amp;Language</source>
        <translation>&amp;Langue</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="331"/>
        <source>&amp;Interface</source>
        <translation>&amp;Interface</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="344"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="351"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="365"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="368"/>
        <source>Create a new game</source>
        <translation>Crée un nouveau jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="371"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="376"/>
        <source>&amp;Load</source>
        <translation>&amp;Charger</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="379"/>
        <source>Load a game</source>
        <translation>Charge un jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="382"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="387"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="390"/>
        <source>Save the current game</source>
        <translation>Sauvegarde le jeu en cours</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="393"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="398"/>
        <source>S&amp;ave as</source>
        <translation>Enregistrer &amp;sous</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="401"/>
        <source>Save the current game in a new file</source>
        <translation>Sauvegarde le jeu en cours dans un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="404"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="409"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="412"/>
        <source>Exit the application</source>
        <translation>Sort de l&apos;application</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="415"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="420"/>
        <source>&amp;About</source>
        <translation>&amp;À propos</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="423"/>
        <source>Display information about GM-Assistant</source>
        <translation>Informations à propos de GM-Assistant</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="426"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="431"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recharger</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="434"/>
        <source>Reload the current game</source>
        <translation>Recharge le jeu en cours</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="442"/>
        <source>R&amp;ecent</source>
        <translation>R&amp;écents</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="445"/>
        <source>Load a recently opened game</source>
        <translation>Charge un jeu ouvert récemment</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="456"/>
        <source>&amp;Full</source>
        <translation>&amp;Complète</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="473"/>
        <source>Tools shown : Music and Sound effects</source>
        <translation>Outils affichés : Musique et Bruitages</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="548"/>
        <source>Throw virtual dices</source>
        <translation>Jette des dés virtuels</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="551"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="556"/>
        <source>&amp;Combat manager</source>
        <translation>&amp;Gestionnaire de combat</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="559"/>
        <source>Manage round per round combats</source>
        <translation>Gère les combats au tour par tour</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="562"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="581"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="570"/>
        <source>&amp;English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="575"/>
        <source>&amp;Metadata</source>
        <translation>&amp;Métadonnées</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="578"/>
        <source>Edit the game metadata</source>
        <translation>Édite les métadonnées du jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="462"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="313"/>
        <source>&amp;Help</source>
        <translation>A&amp;ide</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="459"/>
        <source>Tools shown : Plot, Notes, Characters, History, Music and Sound effects</source>
        <translation>Outils affichés : Intrigue, Notes, Personnages, Historique, Musique et Bruitages</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="470"/>
        <source>&amp;Music</source>
        <translation>&amp;Musique</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="476"/>
        <source>F7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="484"/>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="487"/>
        <source>Tools shown : Plot, Music and Sound effects</source>
        <translation>Outils affichés : Intrigue, Musique et Bruitages</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="490"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="498"/>
        <source>&amp;Design</source>
        <translation>C&amp;onception</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="523"/>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="526"/>
        <source>Undo the last modification</source>
        <translation>Annule la dernière modification</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="529"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="534"/>
        <source>&amp;Redo</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="537"/>
        <source>Redo the last undone modification</source>
        <translation>Refait la dernière modification annulée</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="540"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="545"/>
        <source>&amp;Dice simulator</source>
        <translation>&amp;Simulateur de dés</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="504"/>
        <source>F8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="501"/>
        <source>Tools shown : Plot, Characters and Notes</source>
        <translation>Outils affichés : Intrigue, Personnages et Notes</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="512"/>
        <source>&amp;No Music</source>
        <translation>S&amp;ans son</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="515"/>
        <source>Tools shown : Plot, Characters, History and Notes</source>
        <translation>Outils affichés : Intrigue, Personnages, Historique et Notes</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="518"/>
        <source>F9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.ui" line="437"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="452"/>
        <location filename="../sources/windows/MainWindow.cpp" line="911"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="447"/>
        <source>&amp;Resume</source>
        <translation>&amp;Reprise</translation>
    </message>
    <message>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="95"/>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="109"/>
        <location filename="../sources/widgets/QCustomTreeWidget.cpp" line="502"/>
        <location filename="../sources/windows/MainWindow.cpp" line="303"/>
        <location filename="../sources/windows/MainWindow.cpp" line="309"/>
        <location filename="../sources/windows/MainWindow.cpp" line="350"/>
        <location filename="../sources/windows/MainWindow.cpp" line="389"/>
        <location filename="../sources/windows/MainWindow.cpp" line="498"/>
        <location filename="../sources/windows/MainWindow.cpp" line="536"/>
        <location filename="../sources/windows/MainWindow.cpp" line="547"/>
        <location filename="../sources/windows/MainWindow.cpp" line="553"/>
        <location filename="../sources/windows/MainWindow.cpp" line="853"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="292"/>
        <source>Select the file to open</source>
        <translation>Sélectionnez le fichier à ouvrir</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="292"/>
        <source>GM-Assistant files (*.gms *.gma);;XML files (*.xml)</source>
        <translation>Fichiers GM-Assistant (*.gms *.gma);;Fichiers XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="309"/>
        <location filename="../sources/windows/MainWindow.cpp" line="553"/>
        <source>The game will be loaded anyway, but some features might not work properly.</source>
        <translation>Le jeu va tout de même être chargé, mais certaines fonctionnalités peuvent ne pas fonctionner normalement.</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="309"/>
        <location filename="../sources/windows/MainWindow.cpp" line="553"/>
        <source>The game cannot be loaded correctly for the following reason: </source>
        <translation>Le jeu n&apos;a pas pu être chargé pour la raison suivante :</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="319"/>
        <location filename="../sources/windows/MainWindow.cpp" line="338"/>
        <location filename="../sources/windows/MainWindow.cpp" line="561"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="319"/>
        <location filename="../sources/windows/MainWindow.cpp" line="561"/>
        <source>The syntax of the game you have just loaded is not rigourously correct. Would you like to fix it now?</source>
        <translation>La syntaxe du jeu que vous venez de charger n&apos;est pas rigoureusement correcte. Voulez-vous la corriger maintenant ?</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="338"/>
        <source>The game you want to save does not use the latest version of GM-Assistant files. Do you want to update it? If no, some features may not be saved properly.</source>
        <translation>Le jeu que vous voulez sauvegarder n&apos;utilise pas la dernière version de fichiers GM-Assistant. Voulez-vous le mettre à jour ? Si non, certaines fonctionnalités peuvent ne pas être sauvegardées correctement.</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="357"/>
        <source>GM-Assistant files (1.2) (*.gms)</source>
        <translation>Fichiers GM-Assistant (1.2) (*.gms)</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="360"/>
        <source>;;GM-Assistant files (1.1) (*.gma);;GM-Assistant files (1.0) (*.xml)</source>
        <translation>;;Fichiers GM-Assistant (1.1) (*.gma);;Fichiers GM-Assistant (1.0) (*.xml)</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="362"/>
        <source>Select the file to save</source>
        <translation>Sélectionnez le fichier à sauvegarder</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="498"/>
        <source>Unable to play the file</source>
        <translation>Impossible de jouer le fichier</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="536"/>
        <source>The file &quot;%1&quot; does not exist.</source>
        <translation>Le fichier &quot;%1&quot; n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="793"/>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="793"/>
        <source>The current game has been modified since the last save. If you continue, unsaved changes will be discarded.</source>
        <translation>Le jeu en cours a été modifié depuis la dernière sauvegarde. Si vous continuez, les changements non sauvegardés seront perdus.</translation>
    </message>
    <message>
        <location filename="../sources/windows/MainWindow.cpp" line="718"/>
        <source>New game</source>
        <translation>Nouveau jeu</translation>
    </message>
</context>
<context>
    <name>metadataDialog</name>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="14"/>
        <source>Game metadata</source>
        <translation>Métadonnées du jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="23"/>
        <source>Metadata</source>
        <translation>Métadonnées</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="32"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="42"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="59"/>
        <source>Creation date</source>
        <translation>Date de création</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="66"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="76"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="93"/>
        <source>Game date</source>
        <translation>Date de jeu</translation>
    </message>
    <message>
        <location filename="../sources/windows/MetadataDialog.ui" line="103"/>
        <source>Role-playing game</source>
        <translation>Jeu de rôle</translation>
    </message>
</context>
<context>
    <name>selectCharacterDialog</name>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="14"/>
        <source>Character selection</source>
        <translation>Sélection de personnages</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="25"/>
        <source>All characters</source>
        <translation>Tous les personnages</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="36"/>
        <source>&amp;Add</source>
        <translation>A&amp;jouter</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="47"/>
        <source>&amp;Remove</source>
        <translation>&amp;Retirer</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="63"/>
        <source>Involved characters</source>
        <translation>Personnages impliqués</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="74"/>
        <source>&amp;Up</source>
        <translation>&amp;Haut</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.ui" line="85"/>
        <source>&amp;Down</source>
        <translation>&amp;Bas</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.cpp" line="54"/>
        <source>Uncompleted character selection</source>
        <translation>Sélection des personnages incomplète</translation>
    </message>
    <message>
        <location filename="../sources/windows/SelectCharacterDialog.cpp" line="54"/>
        <source>You have selected too few characters. There must be at least two of them.</source>
        <translation>Vous avez sélectionné trop peu de personnages. Il doit y en avoir au moins deux.</translation>
    </message>
</context>
</TS>
